Automotive Grade Linux (AGL) Continuous Integration (CI) for SanCloud Hardware
==============================================================================

[<img align=right src="https://www.sancloud.co.uk/wp-content/uploads/2016/09/sancloud_and_address_web.png">](https://www.sancloud.co.uk/)

This project supports automated builds of the AGL demo images for SanCloud
devices.

<br />
<br />
