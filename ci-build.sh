#! /bin/bash

set -e

cd agl
ORIG_WORKDIR="`pwd`"
mkdir -p images-bbe

if [[ $# != 1 ]]; then
    echo "ERROR: Too few arguments!"
    echo "Usage: $0 PROFILE"
    echo "    where PROFILE is the AGL profile to build (agl-demo or agl-cluster-demo)"
    exit 1
fi

PROFILE=$1
case ${PROFILE} in
    agl-demo)
        IMAGE=agl-demo-platform
        ;;
    agl-cluster-demo)
        IMAGE=agl-cluster-demo-platform
        ;;
    agl-telematics-demo)
        IMAGE=agl-telematics-demo-platform
        ;;
    *)
        echo "ERROR: Unrecognized profile ${PROFILE}"
        echo "Supported profiles are agl-demo, agl-cluster-demo and agl-telematics-demo"
        exit 1
esac

source meta-agl/scripts/aglsetup.sh -m bbe ${PROFILE} agl-devel

cat >> conf/local.conf << EOF
INHERIT += "archiver"
BB_GENERATE_MIRROR_TARBALLS = "1"
BB_GENERATE_SHALLOW_TARBALLS = "1"
BB_GIT_SHALLOW = "1"
ARCHIVER_MODE[src] = "mirror"
ARCHIVER_MODE[mirror] = "combined"
ARCHIVER_MIRROR_EXCLUDE = "file://"
COPYLEFT_LICENSE_INCLUDE = "*"

PREMIRRORS ??= "\
bzr://.*/.*   https://cdn.sancloud.info/file/sc-yocto/mirror/ \n \
cvs://.*/.*   https://cdn.sancloud.info/file/sc-yocto/mirror/ \n \
git://.*/.*   https://cdn.sancloud.info/file/sc-yocto/mirror/ \n \
gitsm://.*/.* https://cdn.sancloud.info/file/sc-yocto/mirror/ \n \
hg://.*/.*    https://cdn.sancloud.info/file/sc-yocto/mirror/ \n \
osc://.*/.*   https://cdn.sancloud.info/file/sc-yocto/mirror/ \n \
p4://.*/.*    https://cdn.sancloud.info/file/sc-yocto/mirror/ \n \
svn://.*/.*   https://cdn.sancloud.info/file/sc-yocto/mirror/ \n"

MIRRORS =+ "\
ftp://.*/.*      https://cdn.sancloud.info/file/sc-yocto/mirror/ \n \
http://.*/.*     https://cdn.sancloud.info/file/sc-yocto/mirror/ \n \
https://.*/.*    https://cdn.sancloud.info/file/sc-yocto/mirror/ \n"

SSTATE_MIRRORS =+ "\
file://.* https://cdn.sancloud.info/file/sc-yocto/agl/sstate-master/PATH \n"
EOF

# Make output a little quieter but capture the full log to a file
( bitbake --setscene-only ${IMAGE} || true) | tee "${ORIG_WORKDIR}/images-bbe/build-setscene.log" | sed -e '/^NOTE: .*Started$/d' -e '/^NOTE: Running /d'
bitbake --skip-setscene ${IMAGE} | tee "${ORIG_WORKDIR}/images-bbe/build.log" | sed -e '/^NOTE: .*Started$/d' -e '/^NOTE: Running /d'

# Find the deploy directory
DEPLOY_DIR="`bitbake -e | sed -n -e 's/^DEPLOY_DIR="\(.*\)"$/\1/p'`"

# Capture artifacts
cd "${ORIG_WORKDIR}"
cp "${DEPLOY_DIR}/images/bbe/${IMAGE}-bbe.wic.xz" images-bbe/${IMAGE}.wic.xz
cp "${DEPLOY_DIR}/images/bbe/${IMAGE}-bbe.wic.bmap" images-bbe/${IMAGE}.wic.bmap
gzip images-bbe/build-setscene.log
gzip images-bbe/build.log
tar czf images-bbe/licenses.tar.gz -C "${DEPLOY_DIR}" licenses
