#! /bin/bash

set -e

if [[ $# != 1 ]]; then
    echo "ERROR: Too few arguments!"
    echo "Usage: $0 BRANCH"
    echo "    where BRANCH is the AGL branch to fetch"
    exit 1
fi

BRANCH=$1

mkdir -p agl
cd agl
repo init -u https://gerrit.automotivelinux.org/gerrit/AGL/AGL-repo -b $BRANCH
repo sync -d --force-sync
../patches/apply.py
